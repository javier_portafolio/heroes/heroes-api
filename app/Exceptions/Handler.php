<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Support\Facades\RateLimiter;
use App\Enums\HttpStatus;
use Throwable;
use TypeError;
use ErrorException;
use Error;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Throwable $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        // comprueba que la excepcion sea de ThrottleRequests
        if($exception instanceof ThrottleRequestsException) {
            return response()->json([
                'code' => HttpStatus::TOOMANYREQUESTS,
                'error' => true,
                'message' => HttpStatus::TOOMANYREQUESTS(),
            ], HttpStatus::TOOMANYREQUESTS);
        }

        // Comprueba que la excepcion sea de RouteNotFoundException
        if($exception instanceof RouteNotFoundException) {
            return response()->json([
                'code' => HttpStatus::NOTFOUND,
                'error' => true,
                'message' => HttpStatus::NOTFOUND(),
            ], HttpStatus::NOTFOUND);
        }

        // Comprueba que la excepcion sea de MethodNotAllowedHttpException
        if($exception instanceof MethodNotAllowedHttpException) {
            return response()->json([
                'code' => HttpStatus::METHODNOTALLOWED,
                'error' => true,
                'message' => HttpStatus::METHODNOTALLOWED(),
            ], HttpStatus::METHODNOTALLOWED);
        }

        // Comprueba que la excepcion sea de TypeError
        if($exception instanceof TypeError) {
            // El mensaje se controla segun el entorno, si es "production" muestra mensaje generico
            // sino, muestra el error del exception
            return response()->json([
                'code' => HttpStatus::ERROR,
                'error' => true,
                'message' => app()->environment('production') ? HttpStatus::ERROR() : $exception->getMessage(),
            ], HttpStatus::ERROR);
        }

        if($exception instanceof ErrorException) {
            return response()->json([
                'code' => HttpStatus::ERROR,
                'error' => true,
                'message' => app()->environment('production') ? HttpStatus::ERROR() : $exception->getMessage(),
            ]);
        }

        if ($exception instanceof Error) {
            return response()->json([
                'code' => HttpStatus::ERROR,
                'error' => true,
                'message' => app()->environment('production') ? HttpStatus::ERROR() : $exception->getMessage(),
            ]);
        }

        return parent::render($request, $exception);
    }
}
