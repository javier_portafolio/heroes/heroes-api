<?php
/**
 * Enums de codigos HTTP con mensajes
 *
 * @version 1.0
 * @author Javier Martinez
 */

namespace App\Enums;

class HttpStatus
{
    // Attributes
    public const OK = 200;
    public const CREATED = 201;
    public const NOTMODIFIED = 304;
    public const BADREQUEST = 400;
    public const UNAUTHORIZED = 401;
    public const FORBIDDEN = 403;
    public const NOTFOUND = 404;
    public const METHODNOTALLOWED = 405;
    public const NOTACCEPTABLE = 406;
    public const UNPROCESSABLE = 422;
    public const TOOMANYREQUESTS = 429;
    public const ERROR = 500;

    // Methods
    public static function OK ()
    {
        return __('http.ok');
    }

    public static function CREATED ()
    {
        return __('http.created');
    }

    public static function NOTMODIFIED ()
    {
        return __('http.notmodified');
    }

    public static function BADREQUEST ()
    {
        return __('http.badrequest');
    }

    public static function UNAUTHORIZED ()
    {
        return __('http.unauthorized');
    }

    public static function FORBIDDEN ()
    {
        return __('http.forbidden');
    }

    public static function NOTFOUND ()
    {
        return __('http.notfound');
    }

    public static function METHODNOTALLOWED ()
    {
        return __('http.methodnotallowed');
    }

    public static function NOTACCEPTABLE (string $accept = "")
    {
        return __('http.notacceptable', ['accept' => $accept]);
    }

    public static function UNPROCESSABLE ()
    {
        return __('http.unprocessable');
    }

    public static function TOOMANYREQUESTS ()
    {
        return __('http.toomanyrequests');
    }

    public static function ERROR ()
    {
        return __('http.error');
    }
}
