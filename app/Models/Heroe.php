<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Heroe extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'heroes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'nombre',
        'alias',
        'genero_id',
        'ocupacion_id',
        'especialidad',
        'eliminado',
        'fecha_creado',
        'user_creado',
        'fecha_modificado',
        'user_modifica',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'eliminado',
        'user_creado',
        'user_modifica',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the genero that owns the heroe.
     */
    public function genero()
    {
        return $this->belongsTo('App\Models\Genero', 'genero_id', 'id');
    }

    /**
     * Get the ocupacion that owns the heroe.
     */
    public function ocupacion()
    {
        return $this->belongsTo('App\Models\Ocupacion', 'ocupacion_id', 'id');
    }

    /**
     * Get the user that create the record.
     */
    public function userCreado()
    {
        return $this->belongsTo('App\Models\User', 'user_creado', 'id');
    }

    /**
     * Get the user that modified the record.
     */
    public function userModifica()
    {
        return $this->belongsTo('App\Models\User', 'user_modifica', 'id');
    }
}
