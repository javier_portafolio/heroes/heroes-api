<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bitacora';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'accion',
        'entidad',
        'item',
        'data_original',
        'data_nueva',
        'fecha',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the user that owns the record.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * Registro de actividad en bitacora
     *
     * @param App\Models\User $user
     * @param string $accion
     * @param string $entidad
     * @param integer $item
     * @param string $data_original
     * @param string $data_nueva
     * @return void
     */
    public function registoActividad (\App\Models\User $user, string $accion, string $entidad = null, int $item = null, string $data_original = null, string $data_nueva = null)
    {
        $fecha = new \DateTime('now');

        DB::table($this->table)->insert([
            'user_id' => $user->id,
            'accion' => $accion,
            'entidad' => $entidad,
            'item' => $item,
            'data_original' => $data_original,
            'data_nueva' => $data_nueva,
            'fecha' => $fecha,
        ]);
    }
}
