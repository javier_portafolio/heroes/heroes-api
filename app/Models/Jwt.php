<?php

namespace App\Models;

use Firebase\JWT\JWT as TOKEN;
use Illuminate\Support\Str;

class Jwt
{
    public static function getToken($user)
    {
        $key = config('jwt.secret');
        $time = time();

        $payload = array(
            'iss' => config('app.url'), // Quien emite el token
            'iat' => $time, // Tiempo que inició el token
            'exp' => $time + (60 * 60), // Tiempo que expirará el token (+1 hora)
            'user' => $user->codigo,
        );

        $data = collect([
            "token" => TOKEN::encode($payload, $key),
            "refresh_token" => Str::random(4) . '-' . Str::random(4) .'-' . Str::random(8),
        ]);

        return $data;
    }

    public static function validateToken ($request)
    {
        $key = config('jwt.secret');
        $data = collect();

        $token = explode("Bearer ", $request->header('Authorization'));

        try {
            $decode = TOKEN::decode($token[1], $key, array('HS256'));
            $data->put("data", $decode);
        } catch (\Exception $e) {
            $data->put("message", $e->getMessage());
        }

        return $data;
    }
}
