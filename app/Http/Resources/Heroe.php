<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Heroe extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'codigo' => $this->codigo,
            'nombre' => $this->nombre,
            'alias' => $this->alias,
            'genero' => $this->genero->genero,
            'ocupacion' => $this->ocupacion->ocupacion,
            'especialidad' => $this->especialidad,
            'fecha_creado' => $this->fecha_creado,
            'user_creado' => $this->userCreado->name,
            'fecha_modificado' => $this->fecha_modificado,
            'usuario_modifica' => $this->userModifica->name ?? $this->usuario_modifica
        ];
    }
}
