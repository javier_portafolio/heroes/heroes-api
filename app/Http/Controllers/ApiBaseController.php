<?php

namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApiBaseController extends Controller
{
    /**
     * Metodo para pruebas
     *
     * @param array $data
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function JsonResponse (array $data = [])
    {
        $response["code"] = HttpStatus::OK;
        $response["debug"] = true;
        $response["error"] = false;
        $response["body"] = $data;

        return response()->json($response, HttpStatus::OK);
    }

    /**
     * Metodo de control para respuestas 200
     *
     * @param array       $data
     * @param int|integer $code
     * @param string|null $message
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function JsonResponse200 (array $data = [], int $code = 200, string $message = null)
    {
        $response["code"] = $code;
        $response["error"] = false;

        if ($code == 200 && !empty($data)) {
            $response["body"] = $data;
            $response["message"] = $message ?? HttpStatus::OK();
        }

        if ($code == 200 && empty($data) && $message) {
            $response["message"] = $message;
        }

        if ($code == 201) {
            $response["body"] = $data;
            $response["message"] = $message ?? HttpStatus::CREATED();
        }

        if ($code == 304) {
            $response["body"] = $data;
            $response["message"] = $message ?? HttpStatus::NOTMODIFIED();
        }

        if (empty($data) && !$message) {
            $response["code"] = HttpStatus::NOTFOUND;
            $response["message"] = $message ?? HttpStatus::NOTFOUND();
        }
        return response()->json($response, $response['code']);
    }

    // Metodo de control para respuestas 400
    public function JsonResponse400 (array $data = [], string $option = null)
    {
        $response["code"] = HttpStatus::BADREQUEST;
        $response["error"] = true;

        if ($option == "check_parameters") {
            $response["code"] = HttpStatus::UNPROCESSABLE;
            $response["body"] = $data;
            $response["message"] = HttpStatus::UNPROCESSABLE();
        }

        if ($option == "not_found") {
            $response["code"] = HttpStatus::NOTFOUND;
            $response["message"] = HttpStatus::NOTFOUND();
        }

        if ($option == "bad_credentials") {
            $response["message"] = __("auth.failed");
        }

        if ($option == "refresh_invalido") {
            $response["message"] = __("auth.refresh.invalid");
        }

        if ($option == "refresh_expired") {
            $response["message"] = __("auth.refresh.expired");
        }

        if ($option == "confirmation_invalido") {
            $response["message"] = __("auth.confirmation.invalid");
        }

        if ($option == "confirmation_expired") {
            $response["message"] = __("auth.confirmation.expired");
        }

        if ($option == "recovery_invalido") {
            $response["message"] = __("auth.recovery.invalid");
        }

        if ($option == "recovery_expired") {
            $response["message"] = __("auth.recovery.expired");
        }

        if ($option == "forbidden") {
            $response["code"] = HttpStatus::FORBIDDEN;
            $response["message"] = HttpStatus::FORBIDDEN();
        }

        return response()->json($response, $response["code"]);
    }

    /**
     * Metodo de control para respuesta 500
     *
     * @param mix $data
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function JsonResponse500 ($data)
    {
        // condicional para mostrar un error generico o detallado
        $prod = app()->environment('production') ? true : false;
        $response["code"] = HttpStatus::ERROR;
        $response["error"] = true;
        $response["message"] = "";

        // si data es instancia de Exception
        if ($data instanceof \Exception) {
            $response["message"] = $prod ? HttpStatus::ERROR() : $data->getMessage();
        }

        return response()->json($response, $response["code"]);
    }

    /**
     * Metodo para generar los UUID
     *
     * @return uuid
     */
    public function generateUuid ()
    {
        return Str::uuid();
    }
}
