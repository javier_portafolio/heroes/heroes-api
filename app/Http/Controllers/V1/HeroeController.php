<?php

namespace App\Http\Controllers\V1;

use Validator;
use App\Models\Heroe;
use App\Models\Bitacora;
use App\Http\Controllers\ApiBaseController;
use App\Http\Resources\Heroe as HeroeResource;
use Illuminate\Http\Request;

class HeroeController extends ApiBaseController
{
    /**
     * Lista de heroes
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function index (Request $request)
    {
        try {
            $heroes = Heroe::where('eliminado', 0)->get();

            if ($heroes->isEmpty()) {
                return $this->JsonResponse200([]);
            }

            $response['heroes'] = [];

            foreach ($heroes as $heroe) {
                $response['heroes'][] = new HeroeResource($heroe);
            }

            return $this->JsonResponse200($response);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Crear heroe
     *
     * @param Request $request
     * @param Bitacora $bitacora
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function store (Request $request, Bitacora $bitacora)
    {
        try {
            $validacion = Validator::make($request->all(), [
                'nombre' => ['required', 'string', 'min:3', 'max:50', 'regex:/^[a-zA-Z\s]*$/'],
                'alias' => ['required', 'string', 'min:3', 'max:50', 'regex:/^[a-zA-Z\s]*$/'],
                'genero' => ['required', 'integer'],
                'ocupacion' => ['required', 'integer'],
                'especialidad' => ['required', 'string', 'min:3', 'max:200', 'regex:/^[a-zA-Z0-9\s]*$/'],
            ]);

            if ($validacion->fails()) {
                $response = [
                    'errores' => $validacion->errors()->toArray()
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            do {
                $codigo = $this->generateUuid();

                $codigoAux = Heroe::where('codigo', $codigo)->first();
            } while ($codigoAux);

            $fecha = new \DateTime('now');

            $heroe = new Heroe;
            $heroe->codigo = $codigo;
            $heroe->nombre = trim($request->nombre);
            $heroe->alias = trim($request->alias);
            $heroe->genero_id = $request->genero;
            $heroe->ocupacion_id = $request->ocupacion;
            $heroe->especialidad = trim($request->especialidad);
            $heroe->fecha_creado = $fecha;
            $heroe->user_creado = auth()->user()->id;
            $heroe->save();
            $heroe->refresh();

            // registro en bitacora
            $bitacora->registoActividad(auth()->user(), 'registro', 'heroes', $heroe->id, null, json_encode($heroe->toArray()));

            $response = [
                'heroe' => new HeroeResource($heroe),
            ];

            return $this->JsonResponse200($response, 201);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Muestra heroe
     *
     * @param uuid $codigo
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function show ($codigo)
    {
        try {
            $heroe = Heroe::where('eliminado', 0)->where('codigo', $codigo)->first();

            if (!$heroe) {
                return $this->JsonResponse200([]);
            }

            $response['heroe'] = new HeroeResource($heroe);

            return $this->JsonResponse200($response);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Muestra heroe datos para editar
     *
     * @param uuid $codigo
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function edit ($codigo)
    {
        try {
            $heroe = Heroe::where('eliminado', 0)->where('codigo', $codigo)->first();

            if (!$heroe) {
                return $this->JsonResponse200([]);
            }

            if ($heroe->user_creado !== auth()->user()->id) {
                return $this->JsonResponse400([], "forbidden");
            }

            $response['heroe'] = $heroe->toArray();

            return $this->JsonResponse200($response);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Actualizar heroe
     *
     * @param Request $request
     * @param uuid $codigo
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function update (Request $request, $codigo, Bitacora $bitacora)
    {
        try {
            $heroe = Heroe::where('eliminado', 0)->where('codigo', $codigo)->first();

            if (!$heroe) {
                return $this->JsonResponse200([]);
            }

            if ($heroe->user_creado !== auth()->user()->id) {
                return $this->JsonResponse400([], "forbidden");
            }

            $validacion = Validator::make($request->all(), [
                'nombre' => ['required', 'string', 'min:3', 'max:50', 'regex:/^[a-zA-Z\s]*$/'],
                'alias' => ['required', 'string', 'min:3', 'max:50', 'regex:/^[a-zA-Z\s]*$/'],
                'genero' => ['required', 'integer'],
                'ocupacion' => ['required', 'integer'],
                'especialidad' => ['required', 'string', 'min:3', 'max:200', 'regex:/^[a-zA-Z0-9\s]*$/'],
            ]);

            if ($validacion->fails()) {
                $response = [
                    'errores' => $validacion->errors()->toArray()
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            $data_original = $heroe->toArray();
            $fecha = new \DateTime('now');
            $heroe->nombre = trim($request->nombre);
            $heroe->alias = trim($request->alias);
            $heroe->genero_id = $request->genero;
            $heroe->ocupacion_id = $request->ocupacion;
            $heroe->especialidad = trim($request->especialidad);

            if (!$heroe->isDirty()) {
                return $this->JsonResponse200($heroe->toArray(), 200, __('http.notmodified'));
            }

            $heroe->fecha_modificado = $fecha;
            $heroe->user_modifica = auth()->user()->id;
            $heroe->save();
            $heroe->refresh();

            // registro en bitacora
            $bitacora->registoActividad(auth()->user(), 'editar', 'heroes', $heroe->id, json_encode($data_original), json_encode($heroe->toArray()));

            $response = [
                'heroe' => new HeroeResource($heroe),
            ];

            return $this->JsonResponse200($response);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * ELiminar heroe
     *
     * @param uuid $codigo
     * @param Bitacora $bitacora
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function destroy ($codigo, Bitacora $bitacora)
    {
        try {
            $heroe = Heroe::where('eliminado', 0)->where('codigo', $codigo)->first();

            if (!$heroe) {
                return $this->JsonResponse200([]);
            }

            if ($heroe->user_creado !== auth()->user()->id) {
                return $this->JsonResponse400([], "forbidden");
            }

            $heroe->eliminado = true;
            $heroe->save();

            // registro en bitacora
            $bitacora->registoActividad(auth()->user(), 'eliminar', 'heroes', $heroe->id, null, null);

            return $this->JsonResponse200([], 200, __('http.ok'));
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }
}
