<?php

namespace App\Http\Controllers\V1;

use App\Models\Heroe;
use App\Http\Controllers\ApiBaseController;
use Illuminate\Http\Request;

class DashboardController extends ApiBaseController
{
    /**
     * Show all data to build the dashboard
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(Request $request)
    {
        try {
            $total = Heroe::where('eliminado', 0)->count();
            $totalUsuario = Heroe::where('eliminado', 0)->where('user_creado', auth()->user()->id)->count();

            $response['dashboard'] = [
                'total' => $total,
                'totalUsuario' => $totalUsuario
            ];

            return $this->JsonResponse200($response);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }
}
