<?php

namespace App\Http\Controllers\V1;

use App\Models\Genero;
use App\Models\Ocupacion;
use App\Http\Controllers\ApiBaseController;
use Illuminate\Http\Request;

class ReferencialController extends ApiBaseController
{
    /**
     * Lista de generos
     *
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function generos ()
    {
        try {
            $generos = Genero::all();

            if ($generos->isEmpty()) {
                return $this->JsonResponse200([]);
            }

            $response['generos'] = $generos->toArray();

            return $this->JsonResponse200($response);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Lista de ocupaciones
     *
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function ocupaciones ()
    {
        try {
            $ocupaciones = Ocupacion::all();

            if ($ocupaciones->isEmpty()) {
                return $this->JsonResponse200([]);
            }

            $response['ocupaciones'] = $ocupaciones->toArray();

            return $this->JsonResponse200($response);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }
}
