<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Http\Controllers\ApiBaseController;
use App\Models\Jwt as JWT;
use App\Models\User;
use App\Models\Token;
use App\Mail\Register;
use App\Mail\Confirmation;
use App\Mail\Recovery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends ApiBaseController
{
    /**
     * Login general para obtener token
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function login (Request $request)
    {
        try {
            $validacion = Validator::make($request->all(), [
                'email' => ['required', 'email:rfc,dns'],
                'password' => ['required']
            ]);

            if ($validacion->fails()) {
                $response = [
                    'errores' => $validacion->errors()->toArray()
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            $credenciales = $request->only('email', 'password');

            // Intento de inicio de sesion
            if (Auth::once($credenciales)) {
                $user = auth()->user();

                $data = $this->generarToken($user);

                return $this->JsonResponse200($data->toArray(), 200, __('auth.success'));
            }

            return $this->JsonResponse400([], "bad_credentials");
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Login verify, obtener datos del usuario
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function loginVerify (Request $request)
    {
        try {
            $token = explode("Bearer ", $request->header('Authorization'));

            $validacion = Validator::make($request->all(), [
                'email' => ['required', 'email:rfc,dns'],
                'password' => ['required']
            ]);

            if ($validacion->fails()) {
                $response = [
                    'errores' => $validacion->errors()->toArray()
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            $token = JWT::validateToken($request);

            if ($token->has("message")) {
                return $this->JsonResponse400([], "bad_credentials");
            }

            $credenciales = $request->only('email', 'password');
            $credenciales["codigo"] = $token->get('data')->user;

            // Intento de inicio de sesion
            if (Auth::once($credenciales)) {
                $user = auth()->user();

                $data = collect([
                    "usuario" => $user->toArray(),
                ]);

                return $this->JsonResponse200($data->toArray(), 200, __('auth.success'));
            }

            return $this->JsonResponse400([], "bad_credentials");
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Refresh token
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function refresh (Request $request)
    {
        try {
            $fecha = new \DateTime('now');
            $validator = Validator::make($request->all(), [
                'refresh_token' => ['required', 'regex:/^[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{8}$/', Rule::exists('tokens', 'codigo')->where(function ($query) {
                    $query->where('tipo', 'refresh');
                    $query->where('usado', false);
                })],
            ]);

            if ($validator->fails()) {
                $response = [
                    'errores' => $validator->errors()->toArray()
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            $refreshToken = $request->refresh_token;
            $apikey = $request->header('apikey');
            $token = Token::where('codigo', $refreshToken)->first();
            $fechaExpiracion = new \DateTime($token->fecha_expiracion);

            if ($apikey !== $token->user->apikey) {
                return $this->JsonResponse400([], "refresh_invalido");
            }

            // Valida que no este expirado el refresh token
            if ($fecha->format("Y-m-d H:i:s") > $fechaExpiracion->format("Y-m-d H:i:s")) {
                return $this->JsonResponse400([], "refresh_expired");
            }

            $data = $this->generarToken($token->user);

            $token->usado = true;
            $token->save();

            return $this->JsonResponse200($data->toArray(), 200, __('auth.refresh.success'));
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Registrar
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function register (Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'regex:/^([a-zA-Z\s])*$/', 'max:50'],
                'email' => ['required', 'email:rfc,dns', 'unique:users,email'],
                'password' => ['required', 'between:8,18'],
                'password_confirmation' => ['required_with:password', 'same:password'],
            ]);

            if ($validator->fails()) {
                $response = [
                    'errores' => $validator->errors()->toArray()
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            do {
                $codigo = $this->generateUuid();

                $codigoAux = User::where('codigo', $codigo)->first();
            } while ($codigoAux);

            do {
                $apikey = $this->generateApikey();

                $apikeyAux = User::where('apikey', $apikey)->first();
            } while ($apikeyAux);

            $usuario = new User;
            $usuario->name = trim($request->name);
            $usuario->email = trim($request->email);
            $usuario->password = Hash::make($request->password);
            $usuario->apikey = $apikey;
            $usuario->codigo = $codigo;
            $usuario->fecha_registro = new \DateTime();
            $usuario->save();

            $token = $this->generateCodigo($usuario, "registro");
            if ($token) {
                Mail::to($usuario->email)->send(new Register($token));
            }

            $data = collect([
                'usuario' => [
                    'name' => $usuario->name,
                ]
            ]);

            return $this->JsonResponse200($data->toArray(), 201);
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Confirmacion email
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function confirmationEmail (Request $request)
    {
        try {
            $fecha = new \DateTime('now');
            $validator = Validator::make($request->all(), [
                'codigo' => ['required', 'regex:/^[C]{1}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}$/', Rule::exists('tokens', 'codigo')->where(function ($query) {
                    $query->where('tipo', 'confirmation');
                    $query->where('usado', false);
                })],
            ]);

            if ($validator->fails()) {
                $response = [
                    'errores' => $validator->errors()->toArray(),
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            $codigo = $request->codigo;
            $apikey = $request->header('apikey');
            $token = Token::where('codigo', $codigo)->first();
            $fechaExpiracion = new \DateTime($token->fecha_expiracion);

            if ($apikey !== $token->user->apikey) {
                return $this->JsonResponse400([], "confirmation_invalido");
            }

            // Valida que no este expirado el codigo de confirmacion
            if ($fecha->format("Y-m-d H:i:s") > $fechaExpiracion->format("Y-m-d H:i:s")) {
                return $this->JsonResponse400([], "confirmation_expired");
            }

            $token->user->confirmado = true;
            $token->user->fecha_confirmado = new \DateTime('now');
            $token->usado = true;
            $token->user->save();
            $token->save();

            return $this->JsonResponse200([], 200, __('auth.confirmation.success'));
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Enviar confirmacion email
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function confirmationSend (Request $request)
    {
        try {
            $token = $this->generateCodigo(auth()->user(), "confirmacion");
            if ($token) {
                Mail::to(auth()->user()->email)->send(new Confirmation($token));
            }

            return $this->JsonResponse200([], 200, __('auth.confirmation.send'));
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Recuperar contraseña
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function recoveryPassword (Request $request)
    {
        try {
            $fecha = new \DateTime('now');
            $validator = Validator::make($request->all(), [
                'codigo' => ['required', 'regex:/^[R]{1}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}$/', Rule::exists('tokens', 'codigo')->where(function ($query) {
                    $query->where('tipo', 'recovery');
                    $query->where('usado', false);
                })],
                'email' => ['required', 'email:rfc,dns'],
                'password' => ['required', 'between:8,18'],
                'password_confirmation' => ['required_with:password', 'same:password'],
            ]);

            if ($validator->fails()) {
                $response = [
                    'errores' => $validator->errors()->toArray(),
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            $codigo = $request->codigo;
            $token = Token::where('codigo', $codigo)->first();
            $fechaExpiracion = new \DateTime($token->fecha_expiracion);

            if ($request->email !== $token->user->email) {
                return $this->JsonResponse400([], "recovery_invalido");
            }

            // Valida que no este expirado el codigo de recuperacion
            if ($fecha->format("Y-m-d H:i:s") > $fechaExpiracion->format("Y-m-d H:i:s")) {
                return $this->JsonResponse400([], "recovery_expired");
            }

            $token->user->password = Hash::make($request->password);
            $token->usado = true;
            $token->user->save();
            $token->save();

            return $this->JsonResponse200([], 200, __('auth.recovery.success'));
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Enviar recuperacion contraseña
     *
     * @param Request $request
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function recoverySend (Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'email:rfc,dns'],
            ]);

            if ($validator->fails()) {
                $response = [
                    'errores' => $validator->errors()->toArray(),
                ];
                return $this->JsonResponse400($response, "check_parameters");
            }

            $user = User::where('email', $request->email)->first();

            if ($user) {
                $token = $this->generateCodigo($user, "recuperacion");
                if ($token) {
                    Mail::to($user->email)->send(new Recovery($token));
                }
            }

            return $this->JsonResponse200([], 200, __('auth.recovery.send'));
        } catch (\Exception $e) {
            return $this->JsonResponse500($e);
        }
    }

    /**
     * Metodo para crear/refrescar Token Jwt y registrar el refresh_token en bd
     *
     * @param App\Models\User $user
     * @return Collect $data
     */
    private function generarToken ($user)
    {
        $data = JWT::getToken($user);
        $expedicion = new \DateTime('now');
        $expiracion = new \DateTime('now');

        // guardar el refresh_token del usuario
        $token = new Token();
        $token->user_id = $user->id;
        $token->tipo = "refresh";
        $token->codigo = $data->get("refresh_token");
        $token->fecha_expedicion = $expedicion;
        $token->fecha_expiracion = $expiracion->modify("+2 hours");
        $token->save();

        // retorna el token creado
        return $data;
    }

    /**
     * Metodo para generar apikey del usuario
     *
     * @return string
     */
    private function generateApikey ()
    {
        return Str::random(80);
    }

    /**
     * Metodo para generar los codigos de confirmacion
     * y recuperacion de contraseña
     *
     * @return string
     */
    private function generateCodigo (User $usuario, string $objetivo)
    {
        try {
            $aux = "";
            $tipo = "";
            $expireIn = "";
            switch ($objetivo) {
                case 'registro':
                case 'confirmacion':
                    $aux = "C";
                    $tipo = "confirmation";
                    $expireIn = "+2 days";
                    break;

                case 'recuperacion':
                    $aux = "R";
                    $tipo = "recovery";
                    $expireIn = "+5 minutes";
                    break;

                default:
                    return null;
                    break;
            }

            $codigo = $aux . '-' . Str::random(4) . '-' . Str::random(4) .'-' . Str::random(4);
            $expedicion = new \DateTime('now');
            $expiracion = new \DateTime('now');

            $token = new Token;
            $token->user_id = $usuario->id;
            $token->tipo = $tipo;
            $token->codigo = $codigo;
            $token->fecha_expedicion = $expedicion;
            $token->fecha_expiracion = $expiracion->modify($expireIn);
            $token->save();

            return $token;
        } catch (\Exception $e) {
            return null;
        }
    }
}
