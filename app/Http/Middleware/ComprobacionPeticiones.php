<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Enums\HttpStatus;

class ComprobacionPeticiones
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->isMethod('options')) {
            return response()->json([
                'code' => HttpStatus::METHODNOTALLOWED,
                'error' => true,
                'message' => HttpStatus::METHODNOTALLOWED(),
            ], HttpStatus::METHODNOTALLOWED);
        }

        return $next($request);
    }
}
