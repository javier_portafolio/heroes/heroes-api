<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Enums\HttpStatus;

class Authorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->header('Authorization')) {
            return response()->json([
                'code' => HttpStatus::UNAUTHORIZED,
                'error' => true,
                'message' => HttpStatus::UNAUTHORIZED()
            ], HttpStatus::UNAUTHORIZED);
        }

        $authorization = explode(" ", $request->header('Authorization'));

        if ($authorization[0] !== "Bearer") {
            return response()->json([
                'code' => HttpStatus::BADREQUEST,
                'error' => true,
                'message' => __('http.authorization')
            ], HttpStatus::BADREQUEST);
        }

        $token = explode(".", $authorization[1]);

        if (count($token) !== 3) {
            return response()->json([
                'code' => HttpStatus::BADREQUEST,
                'error' => true,
                'message' => __('http.authorization')
            ], HttpStatus::BADREQUEST);
        }

        return $next($request);
    }
}
