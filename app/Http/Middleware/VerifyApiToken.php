<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Enums\HttpStatus;
use Illuminate\Support\Str;

class VerifyApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->header('apikey')) {
            return response()->json([
                'code' => HttpStatus::BADREQUEST,
                'error' => true,
                'message' => __('auth.apikey.not')
            ], HttpStatus::BADREQUEST);
        }

        if (Str::length($request->header('apikey')) !== 80) {
            return response()->json([
                'code' => HttpStatus::BADREQUEST,
                'error' => true,
                'message' => __('auth.apikey.limit')
            ], HttpStatus::BADREQUEST);
        }

        return $next($request);
    }
}
