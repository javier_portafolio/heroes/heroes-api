<?php

namespace App\Http\Middleware;

use Closure;
use App\Enums\HttpStatus;
use App\Models\Jwt as JWT;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyJwtToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param boolean $refresh
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $refresh = false)
    {
        $token = JWT::validateToken($request);

        if ($token->has('message') && $token->get('message') == "Expired token") {
            if (!$refresh) {
                return response()->json([
                    'code' => HttpStatus::UNAUTHORIZED,
                    'error' => true,
                    'message' => __('auth.jwt.expired')
                ], HttpStatus::UNAUTHORIZED);
            }
        }
        else if ($token->has('message')) {
            return response()->json([
                'code' => HttpStatus::ERROR,
                'error' => true,
                'message' => "Token " . $token->get('message')
            ], HttpStatus::ERROR);
        }

        if (!$refresh) {
            $user = User::where('codigo', $token->get('data')->user)->first();

            if (!$user) {
                return response()->json([
                    'code' => HttpStatus::BADREQUEST,
                    'error' => true,
                    'message' => __("auth.failed")
                ], HttpStatus::BADREQUEST);
            }

            Auth::onceUsingId($user->id);
        }

        return $next($request);
    }
}
