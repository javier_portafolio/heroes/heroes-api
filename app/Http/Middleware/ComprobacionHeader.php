<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Enums\HttpStatus;

class ComprobacionHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // obtiene todos los header Accept de la solicitud
        $accept = explode(",", $request->header('Accept'));

        // si en el array no se encuentra "application/json"
        // no se puede proceder
        if (!in_array("application/json", $accept)) {
            return response()->json([
                'code' => HttpStatus::NOTACCEPTABLE,
                'error' => true,
                'message' => HttpStatus::NOTACCEPTABLE("application/json")
            ], HttpStatus::NOTACCEPTABLE);
        }

        return $next($request);
    }
}
