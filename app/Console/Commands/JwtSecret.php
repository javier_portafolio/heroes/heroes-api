<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class JwtSecret extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'key:jwt-secret';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generar llave para JWT';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $key = Str::random(80);

        if (file_exists($path = $this->laravel->environmentFilePath()) === false) {
            return $this->error(".env does not exist");
        }

        if (Str::contains(file_get_contents($path), 'JWT_SECRET') === false) {
            // create new entry
            file_put_contents($path, PHP_EOL."JWT_SECRET=$key".PHP_EOL, FILE_APPEND);

            return $this->info("jwt secret set successfully.");
        }
        else if (Str::length($this->laravel['config']['jwt.secret']) == 0) {
            // update existing entry
            file_put_contents($path, str_replace(
                'JWT_SECRET=',
                'JWT_SECRET='.$key, file_get_contents($path)
            ));

            return $this->info("jwt secret set successfully.");
        }

        return $this->info("jwt secret already exist.");
    }
}
