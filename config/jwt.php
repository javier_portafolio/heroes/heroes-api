<?php

return [
    /*
    |--------------------------------------------------------------------------
    | JWT Secret
    |--------------------------------------------------------------------------
    |
    | This key is used by the Firebase JWT to encode all the TOKEN's
    | a random, 80 character string, must be set with the command 'key:jwt-secret'
    | otherwise the api route does not work!
    |
    */

    'secret' => env('JWT_SECRET'),
];
