<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Enums\HttpStatus;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\V1\HeroeController;
use App\Http\Controllers\V1\ReferencialController;
use App\Http\Controllers\V1\DashboardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Rutas de autorizacion, confirmacion de correo y recuperacion de contraseña
Route::group(['prefix' => 'auth', 'middleware' => ['headers']], function () {

    Route::post('login', [AuthController::class, 'login'])->name('auth.login');

    Route::post('verify', [AuthController::class, 'loginVerify'])->name('auth.verify')->middleware('autorizacion', 'jwt');

    Route::post('refresh', [AuthController::class, 'refresh'])->name('auth.refresh')->middleware(['autorizacion', 'apikey', 'jwt:true']);

    Route::post('register', [AuthController::class, 'register'])->name('auth.register');

    Route::post('confirmation', [AuthController::class, 'confirmationEmail'])->name('auth.confirmation.email')->middleware(['autorizacion', 'apikey', 'jwt']);

    Route::get('confirmation', [AuthController::class, 'confirmationSend'])->name('auth.confirmation.send')->middleware(['autorizacion', 'apikey', 'jwt']);

    Route::post('recovery', [AuthController::class, 'recoveryPassword'])->name('auth.recovery.pass');

    Route::post('recovery/send', [AuthController::class, 'recoverySend'])->name('auth.recovery.send');

    Route::get('token_validate', function () {})->name('auth.token_validate')->middleware(['autorizacion', 'jwt']);

});

Route::group(['prefix' => 'v1', 'middleware' => ['headers', 'autorizacion', 'apikey', 'jwt']], function () {

    // Rutas gestion heroes
    Route::prefix('heroes')->group(function (){

        Route::get('', [HeroeController::class, 'index'])->name('heroes.index');

        Route::post('', [HeroeController::class, 'store'])->name('heroes.store');

        Route::get('{codigo}', [HeroeController::class, 'show'])->name('heroes.show');

        Route::get('{codigo}/edit', [HeroeController::class, 'edit'])->name('heroes.edit');

        Route::put('{codigo}', [HeroeController::class, 'update'])->name('heroes.update');

        Route::delete('{codigo}', [HeroeController::class, 'destroy'])->name('heroes.destroy');

    });

    Route::prefix('referentials')->group(function () {

        Route::get('genders', [ReferencialController::class, 'generos'])->name('referenciales.genero');

        Route::get('occupations', [ReferencialController::class, 'ocupaciones'])->name('referenciales.ocupacion');

    });

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
});

Route::fallback(function () {
    return response()->json([
        'code' => HttpStatus::NOTFOUND,
        'error' => true,
        'message' => HttpStatus::NOTFOUND()
    ], HttpStatus::NOTFOUND);
});
