# Heroes App API

_Gestion de Heroes y Villanos, un CRUD basico inspirado en el [Tour de Heroes](https://angular.io/tutorial) de Angular._

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

### Pre-requisitos 📋

* [Git](https://git-scm.com/)
* [Composer](https://getcomposer.org/)
* [PHP >= 7.2.5](https://www.php.net/) - Con los modulos
	- BCMath PHP Extension
	- Ctype PHP Extension
	- Fileinfo PHP extension
	- JSON PHP Extension
	- Mbstring PHP Extension
	- OpenSSL PHP Extension
	- PDO PHP Extension
	- Tokenizer PHP Extension
	- XML PHP Extension

Descargar o clonar el proyecto por la via de su preferencia.

### Instalación 🔧

Teniendo la copia del repositorio en local, es momento de descargar las dependencias para hacerlo funcionar.

En la terminal/cmd moverse a la ubicancion del proyecto y ejecutar.

```bash
composer install
```

En la raiz del directorio ejecutar

```bash
cp .env.example .env
```

En el archivo `.env` colocar las credenciales que la aplicacion usara.

```
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null

JWT_SECRET=
```

**NOTA:** Para modo produccion comentar las variables `APP_ENV` y `APP_DEBUG`.

## Despliegue 📦

Para cargar los datos importantes en Base de Datos, ejecutar.

```bash
php artisan migrate
```

Generar llave de la aplicacion

```bash
php artisan key:generate
```

Generar llave para JWT

```bash
php artisan key:jwt-secret
```

Y levantar el servidor de artisan con.

```bash
php artisan serve
```

_Si no existe ningun error la aplicacion estara corriendo en [localhost:8000](http://localhost:8000) ya se puede facilitar la direccion al proyecto del front._

## Construido con 🛠️

* [Laravel](https://laravel.com/)

## Interfaces graficas

_Las siguientes son interfaces funcionales para interactuar con el api._

* [Con Angular](https://gitlab.com/javier_portafolio/heroes/heroes-web)
* [Con Vue.js](https://gitlab.com/javier_portafolio/heroes/heroes-web-vue)
