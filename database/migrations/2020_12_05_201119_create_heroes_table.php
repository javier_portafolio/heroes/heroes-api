<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeroesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heroes', function (Blueprint $table) {
            $table->id();
            $table->uuid('codigo')->unique();
            $table->string('nombre', 50);
            $table->string('alias', 50);
            $table->integer('genero_id');
            $table->integer('ocupacion_id');
            $table->text('especialidad');
            $table->boolean('eliminado')->default(0);
            $table->timestamp('fecha_creado');
            $table->integer('user_creado');
            $table->timestamp('fecha_modificado')->nullable()->default(null);
            $table->integer('user_modifica')->nullable();

            $table->index(["genero_id"], 'fk_heroes_generos_idx');

            $table->index(["ocupacion_id"], 'fk_heroes_ocupaciones_idx');

            $table->index(["user_creado"], 'fk_heroes_users_idx');

            $table->index(["user_modifica"], 'fk_heroes_users1_idx');


            $table->foreign('genero_id', 'fk_heroes_generos_idx')
                ->references('id')->on('generos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ocupacion_id', 'fk_heroes_ocupaciones_idx')
                ->references('id')->on('ocupaciones')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_creado', 'fk_heroes_users_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_modifica', 'fk_heroes_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heroes');
    }
}
