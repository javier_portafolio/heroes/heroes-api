<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('accion', 45);
            $table->string('entidad', 45)->nullable();
            $table->integer('item')->nullable();
            $table->text('data_original')->nullable();
            $table->text('data_nueva')->nullable();
            $table->timestamp('fecha');

            $table->index(["user_id"], 'fk_bitacora_usuarios_idx');


            $table->foreign('user_id', 'fk_bitacora_usuarios_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora');
    }
}
