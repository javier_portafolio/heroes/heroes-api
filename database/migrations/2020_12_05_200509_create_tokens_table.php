<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('tipo', 30);
            $table->string('codigo', 80);
            $table->boolean('usado')->default(0);
            $table->timestamp('fecha_expedicion');
            $table->timestamp('fecha_expiracion');

            $table->index(["user_id"], 'fk_tokens_usuarios_idx');


            $table->foreign('user_id', 'fk_tokens_usuarios_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokens');
    }
}
