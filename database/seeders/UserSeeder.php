<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "lisa simpson",
            'email' => "lsimpson@gmail.com",
            'password' => Hash::make("pandas"),
            'apikey' => Str::random(80),
            'codigo' => Str::uuid(),
            'fecha_registro' => new \DateTime(),
        ]);
    }
}
