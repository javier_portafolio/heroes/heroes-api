<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generos')->insert([
            'genero' => "Femenino",
        ]);

        DB::table('generos')->insert([
            'genero' => "Masculino",
        ]);
    }
}
