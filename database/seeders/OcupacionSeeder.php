<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class OcupacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ocupaciones')->insert([
            'ocupacion' => "Heroe",
        ]);

        DB::table('ocupaciones')->insert([
            'ocupacion' => "Villano",
        ]);
    }
}
