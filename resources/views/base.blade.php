<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name') }}</title>

    <style>
        body {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
        }

        #td-logo {
            padding: 20px 0 15px 0;
            background-color: #f4f6f9;
        }

        #logo {
            display: block;
        }

        #content {
            padding: 40px 30px 40px 30px;
            background-color: #343a40;
            color: white;
        }

        #content-header {
            font-family: Arial, sans-serif;
            font-size: 24px;
        }

        .content-body {
            font-family: Arial, sans-serif;
            font-size: 16px;
            line-height: 20px;
        }

        #footer {
            padding: 30px 30px 30px 30px;
            width: 75%;
            background-color: #f4f6f9;
            color: black;
            font-family: Arial, sans-serif;
            font-size: 14px;
        }
    </style>
</head>
<body>

    <table align="center" cellpadding="0" cellspacing="0" width="600">

        <tr>

            <td id="td-logo" align="center">

                <img id="logo" src="{{ asset('img/logo200x200.png') }}" alt="logo heroes app">

            </td>

        </tr>

        <td id="content">

            <table cellpadding="0" cellspacing="0" width="100%">

                <tr id="content-header">

                    <td>@yield('content-header')</td>

                </tr>

                <tr class="content-body">

                    <td style="padding: 20px 0 30px 0;">
                        @yield('content-body')
                    </td>

                </tr>

                <tr class="content-body">

                    <td>Saludos cordiales.</td>

                </tr>

            </table>

        </td>

        <tr>

            <td id="footer">
                Muestra de habilidades adquiridas.<br>
                Esta cuenta de correo no es monitoreada, solo es para fines de desarrollo.<br>
                2020 - {{ date('Y') }}.
            </td>

        </tr>

    </table>

</body>
</html>
