@extends('base')

@section('content-header')
    Recuperar contraseña
@endsection

@section('content-body')
    El presente es para informale que debe ingresar el siguiente código: <strong>{{ $token->codigo }}</strong>,
    para recuperar su contraseña.
@endsection
