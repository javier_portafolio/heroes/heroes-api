@extends('base')

@section('content-header')
    Bienvenido a {{ config('app.name') }}
@endsection

@section('content-body')
    El presente es para informale que debe ingresar el siguiente código: <strong>{{ $token->codigo }}</strong>,
    para confirmar su dirección de correo.
@endsection
