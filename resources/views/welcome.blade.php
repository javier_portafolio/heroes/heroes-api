<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

        <title>{{config('app.name')}}</title>

        <style>
            .contenedor {
                height: auto;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            .columna {
                width: auto;
            }
        </style>
    </head>
    <body>
        <div class="contenedor">
            <div class="columna">
                <img id="logo" src="{{ asset('img/logo200x200.png') }}" alt="logo heroes app">
                <h1>{{config('app.name')}} API</h1>
            </div>
        </div>
    </body>
</html>
