<?php

/**
 * Mensajes para las respuestas HTTP [en]
 *
 * @version 1.0
 * @author Javier Martinez
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Http Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple http responses. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // http responses
    'ok' => 'Successful request.',
    'created' => 'Record created.',
    'notmodified' => 'Record not modified.',
    'badrequest' => 'Bad request.',
    'unauthorized' => 'Unauthorized.',
    'forbidden' => 'You do not have permission to continue.',
    'notfound' => 'Resource not found.',
    'methodnotallowed' => 'Method not allowed.',
    'notacceptable' => 'Doesn\'t found the Accept header: :accept',
    'unprocessable' => 'The entity is not processable.',
    'toomanyrequests' => 'Many requests in a short time.',
    'error' => 'Whoops! Something went wrong.',

    // fail composition of Authorization
    'authorization' => 'Authorization value error.',

];
