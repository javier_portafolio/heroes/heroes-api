<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'success' => 'Successful login.',
    'apikey.not' => 'The apikey is not found.',
    'apikey.limit' => 'The apikey doesn\'t meet the parameters.',
    'jwt.expired' => 'Token expired.',
    'refresh.expired' => 'The refresh token has expired, please login again.',
    'refresh.invalid' => 'The refresh token is invalid.',
    'refresh.success' => 'Refreshed token.',
    'confirmation.expired' => 'The confirmation code has expired.',
    'confirmation.invalid' => 'The confirmation code is invalid.',
    'confirmation.send' => 'The confirmation code has been send.',
    'confirmation.success' => 'The email has been confirmed.',
    'recovery.expired' => 'The recovery code has expired.',
    'recovery.invalid' => 'The recovery code is invalid.',
    'recovery.send' => 'The recovery code has been send.',
    'recovery.success' => 'Your password has been recovered.',

];
