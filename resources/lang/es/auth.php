<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor inténtelo de nuevo en :seconds segundos.',
    'success' => 'Inicio de sesión exitoso.',
    'apikey.not' => 'No se encuentra el apikey.',
    'apikey.limit' => 'El apikey no cumple con los parametros.',
    'jwt.expired' => 'Token expirado.',
    'refresh.expired' => 'El refresh token ha expirado, inicie sesión nuevamente.',
    'refresh.invalid' => 'El refresh token es inválido.',
    'refresh.success' => 'Token refrescado.',
    'confirmation.expired' => 'El codigo de confirmación ha expirado.',
    'confirmation.invalid' => 'El codigo de confirmación es inválido.',
    'confirmation.send' => 'El codigo de confirmación ha sido enviado.',
    'confirmation.success' => 'El correo ha sido confirmado.',
    'recovery.expired' => 'El codigo de recuperación ha expirado.',
    'recovery.invalid' => 'El codigo de recuperación es inválido.',
    'recovery.send' => 'El codigo de recuperación ha sido enviado.',
    'recovery.success' => 'Su contraseña ha sido recuperada.',

];
