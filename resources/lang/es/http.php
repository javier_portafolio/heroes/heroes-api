<?php

/**
 * Mensajes para las respuestas HTTP [es]
 *
 * @version 1.0
 * @author Javier Martinez
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Http Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple http responses. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // respuestas http
    'ok' => 'Solicitud exitosa.',
    'created' => 'Registro creado.',
    'notmodified' => 'Registro no modificado.',
    'badrequest' => 'Solicitud incorrecta.',
    'unauthorized' => 'No autorizado.',
    'forbidden' => 'No tiene permiso para continuar.',
    'notfound' => 'Recurso no encontrado.',
    'methodnotallowed' => 'Método no permitido.',
    'notacceptable' => 'No se encuentra el header Accept: :accept',
    'unprocessable' => 'La entidad no es procesable.',
    'toomanyrequests' => 'Muchas solicitudes en corto tiempo.',
    'error' => '¡Ups! Algo salio mal.',

    // falla de composicion Authorization
    'authorization' => 'Error en valor Autorización.',
];
